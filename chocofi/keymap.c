/*
  Copyright 2019 @foostan
  Copyright 2020 Drashna Jaelre <@drashna>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

  - https://docs.qmk.fm/#/keycodes
  - https://docusaurus.qmk.fm/feature_layers/
  - use LT(layer, kc) - momentarily activates layer when held, and sends kc when tapped.
*/

#include QMK_KEYBOARD_H
#include "keymap_german.h"

// Layer declarations
enum {
  COLEMAK = 0,
  NAV_FUN,
  NUM_SYM,
  MOUSEMO,

  NUM_LAYERS
};

// Left-hand home row mods
#define HGUI_A LGUI_T(KC_A)
#define HALT_R LALT_T(KC_R)
#define HSFT_S LSFT_T(KC_S)
#define HCTL_T LCTL_T(KC_T)

// Left hand Homerow on NAV_FUN layer
#define HGUI_AT LGUI_T(DE_AT)
#define HALT_F4 LALT_T(KC_F4)
#define HSFT_F5 LSFT_T(KC_F5)
#define HCTL_F6 LCTL_T(KC_F6)

// Right-hand home row mods
#define HCTL_N RCTL_T(KC_N)
#define HSFT_E RSFT_T(KC_E)
#define HALT_I LALT_T(KC_I)
#define HGUI_O RGUI_T(KC_O)

// MOUSEMO on right hand index M
#define HMOU_M LT(MOUSEMO, KC_M)
#define HMOU_LEFT LT(MOUSEMO, KC_LEFT)

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
  [COLEMAK] = LAYOUT_split_3x5_3
  (
   KC_Q,       KC_W,    KC_F,    KC_P,    KC_B,       KC_J,    KC_L,     KC_U,    DE_Z, KC_BSPC,
   HGUI_A,   HALT_R,  HSFT_S,  HCTL_T,    KC_G,     HMOU_M,  HCTL_N,   HSFT_E,  HALT_I,  HGUI_O,
   DE_Y,       KC_X,    KC_C,    KC_D,    KC_V,       KC_K,    KC_H,  DE_COMM,  DE_DOT, DE_SLSH,
   LSFT_T(KC_DEL),  LT(NAV_FUN, KC_ESC),  LSFT_T(KC_TAB),    CTL_T(KC_SPC), LT(NUM_SYM, KC_ENT), TG(NAV_FUN)
   ),

  [NAV_FUN] = LAYOUT_split_3x5_3
  (
   DE_BSLS,   KC_F7,   KC_F8,   KC_F9,  KC_F10,    LCTL(KC_F), KC_HOME,  KC_END, RCS(KC_F),    KC_BSPC,
   HGUI_AT, HALT_F4, HSFT_F5, HCTL_F6,  KC_F11,    HMOU_LEFT,  KC_DOWN,   KC_UP,  KC_RIGHT,     KC_DEL,
   DE_AT,     KC_F1,   KC_F2,   KC_F3,  KC_F12,    LCTL(KC_H), KC_PGDN, KC_PGUP, RCS(KC_H), LCTL(KC_P),
   _______, _______,  _______,                     _______, _______, _______
   ),

  [NUM_SYM] = LAYOUT_split_3x5_3
  (
   DE_EXLM, DE_DQUO, DE_QUES, DE_PERC,  DE_DLR,    DE_CIRC,    KC_7,     KC_8,    KC_9, DE_PLUS,
   DE_LABK, DE_LCBR, DE_LPRN, DE_LBRC, DE_QUOT,    DE_ASTR,    KC_4,     KC_5,    KC_6, DE_MINS,
   DE_RABK, DE_RCBR, DE_RPRN, DE_RBRC, DE_HASH,    KC_0,       KC_1,     KC_2,    KC_3,  DE_EQL,
   DE_PIPE, DE_TILD, DE_AMPR,                      _______, _______, DE_DOT
   ),

  [MOUSEMO] = LAYOUT_split_3x5_3
  (
   XXXXXXX, XXXXXXX, KC_MS_U, XXXXXXX, XXXXXXX,    XXXXXXX, XXXXXXX,  DE_UDIA, XXXXXXX, XXXXXXX,
   DE_ADIA, KC_MS_L, KC_MS_D, KC_MS_R, KC_BTN3,    _______, KC_VOLD,  KC_MUTE, KC_VOLU, DE_ODIA,
   XXXXXXX, KC_WH_L, XXXXXXX, KC_WH_R, XXXXXXX,    KC_MPRV, KC_MSTP,  KC_MPLY, KC_MNXT, XXXXXXX,
   _______, KC_BTN1, KC_WH_D,                      KC_WH_U, KC_BTN2, _______
   )
};
