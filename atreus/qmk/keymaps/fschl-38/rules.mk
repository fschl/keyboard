LAYER_LOCK_ENABLE = yes
MOUSEKEY_ENABLE = yes    # Mouse keys

# https://docs.qmk.fm/features/key_overrides
# KEY_OVERRIDE_ENABLE = yes

# https://docs.qmk.fm/features/combo
COMBO_ENABLE = yes

# not enough space on the controller for caps-word feature :(
# https://docs.qmk.fm/features/caps_word#caps-word
CAPS_WORD_ENABLE = yes
