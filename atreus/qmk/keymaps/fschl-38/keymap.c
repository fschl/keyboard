// Copyright (C) 2019, 2020  Keyboard.io, Inc
//
// this is the style you want to emulate.
// This is the canonical layout file for the Quantum project. If you want to add another keyboard,

#include QMK_KEYBOARD_H
#include "keymap_german.h"

enum {
    COLEMAK = 0,
    NUM_SYM,
    NAV_FUN,
    MOUSEMO,

    NUM_LAYERS
};

/*
 * About Layer switching:
 * https://docs.qmk.fm/feature_layers#switching-and-toggling-layers
 */

// Left-hand thumb keys
#define THUMB1 LSFT_T(KC_DEL)
#define THUMB2 LT(NAV_FUN, KC_ESC)
#define THUMB3 LGUI_T(KC_TAB) // TODO: think about removing hold-mod

// right-hand thumb keys
#define THUMB4 CTL_T(KC_SPC) // TODO: think about removing hold-mod
#define THUMB5 LT(NUM_SYM, KC_ENT)
#define THUMB6 LT(NAV_FUN, KC_BSPC)

// Left-hand home row mods
#define HGUI_A LGUI_T(KC_A)
#define HALT_R LALT_T(KC_R)
#define HSFT_S LSFT_T(KC_S)
#define HCTL_T LCTL_T(KC_T)

#define HGUI_AT LGUI_T(KC_AT)
#define HALT_F4 LALT_T(KC_F4)
#define HSFT_F5 LSFT_T(KC_F5)
#define HCTL_F6 LCTL_T(KC_F6)

// Right-hand home row mods
#define HCTL_N RCTL_T(KC_N)
#define HSFT_E RSFT_T(KC_E)
#define HALT_I LALT_T(KC_I)
#define HGUI_O RGUI_T(KC_O)

// MOUSEMO on right hand index M
#define HMOU_M LT(MOUSEMO, KC_M)

// default bottom row
/* KC_ESC, KC_TAB, KC_LGUI,  KC_LSFT, KC_BSPC,  KC_LCTL, KC_LALT, KC_SPC,  MO(_RS), KC_MINS, KC_QUOT, KC_ENT ), */

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
  [COLEMAK] = LAYOUT( /* Colema-DH */
    KC_Q,    KC_W,    KC_F,    KC_P,    KC_B,                      KC_J,    KC_L,    KC_U,    KC_Z,    KC_BSPC,
    KC_A,  HALT_R,  HSFT_S,  HCTL_T,  KC_G,                      HMOU_M,  HCTL_N,  HSFT_E,  HALT_I,  KC_O,
    /* HGUI_A,  HALT_R,  HSFT_S,  HCTL_T,  KC_G,                      HMOU_M,  HCTL_N,  HSFT_E,  HALT_I,  HGUI_O, */
    KC_Y,    KC_X,    KC_C,    KC_D,    KC_V,   KC_HASH, KC_BSLS, KC_K,    KC_H,    KC_COMM, KC_DOT,  KC_SLSH ,
    XXXXXXX, XXXXXXX, XXXXXXX, THUMB1,  THUMB2,  THUMB3,  THUMB4,  THUMB5,  THUMB6,  XXXXXXX, XXXXXXX, XXXXXXX),

  /*
   * Improve: use https://docs.qmk.fm/features/layer_lock to quickly insert one key from this layer
   * and have the option to lock into it, when inserting longer numbers/math expressions. This
   * also makes two right thumb keys available.
   *  !      "     §     $    %          ||             / (\)   7     8     9    +
   *  <      {     (     [    '          ||             *       4     5     6    - (_)
   *  >      }     )     ]    @   LOCK   ||     ,       .       1     2     3    =
   *                     |    ~     &    ||     ^   LAYER       0
   */
  [NUM_SYM] = LAYOUT(
   DE_EXLM, DE_DQUO, DE_QUES, DE_PERC, DE_DLR,                     DE_SLSH,  KC_7,  KC_8,    KC_9,   DE_PLUS,
   DE_LABK, DE_LCBR, DE_LPRN, DE_LBRC, DE_QUOT,                    DE_ASTR,  KC_4,  KC_5,    KC_6,   DE_MINS,
   DE_RABK, DE_RCBR, DE_RPRN, DE_RBRC, DE_AT,   QK_LLCK,  KC_COMM, KC_DOT,   KC_1,  KC_2,    KC_3,   DE_EQL,
   XXXXXXX, XXXXXXX, XXXXXXX, DE_PIPE, DE_TILD, DE_AMPR,  KC_CIRC, _______,  DE_0, XXXXXXX, XXXXXXX, XXXXXXX),

  /*
   * TODO: this is an idea and does NOT represent the implementation below
   *   ´     F7    F8    F9   F10        ||         INS   HOME  UP  END  PgUp
   *   `     F4    F5    F6   F11        ||         Left  Down  Up  Right PgDown
   *         F1    F2    F3   F12  LOCK  || VolUp   Prev  Stop  Pause Play Next
   *                  XXXXX  _____ pause || VolDown Print XXXXX
   */
  [NAV_FUN] = LAYOUT(
   DE_BSLS, KC_F7,   KC_F8,   KC_F9,  KC_F10,                    LCTL(KC_F), KC_HOME,  KC_END, RCS(KC_F),   KC_PGUP,
   HGUI_AT, HALT_F4, HSFT_F5, HCTL_F6, KC_F11,                   KC_LEFT,  KC_DOWN,   KC_UP,  KC_RIGHT,    KC_PGDN,
   DE_AT,    KC_F1,   KC_F2,   KC_F3,  KC_F12, XXXXXXX, XXXXXXX, LCTL(KC_H), KC_PGDN, KC_PGUP, RCS(KC_H), LCTL(KC_P),
   XXXXXXX, XXXXXXX, XXXXXXX, _______, _______, _______, _______, _______, _______,  XXXXXXX, XXXXXXX, XXXXXXX),


  [MOUSEMO] = LAYOUT(
   DE_EURO, DE_SS  , KC_MS_U, XXXXXXX, XXXXXXX,                    XXXXXXX, XXXXXXX,  DE_UDIA, XXXXXXX, XXXXXXX,
   DE_ADIA, KC_MS_L, KC_MS_D, KC_MS_R, KC_BTN3,                    _______, KC_VOLD,  KC_MUTE, KC_VOLU, DE_ODIA,
   XXXXXXX, KC_WH_L, XXXXXXX, KC_WH_R, XXXXXXX, XXXXXXX, XXXXXXX,  KC_MPRV, KC_MSTP,  KC_MPLY, KC_MNXT, XXXXXXX,
   _______, _______, _______, _______, KC_BTN1, KC_WH_D,  KC_WH_U, KC_BTN2, _______, _______, _______, _______)
};

// https://docs.qmk.fm/features/combo#combos
/*  - Umlauts ä+ö+ü are combos with a near key */
/*    - ä = w+a; ö = z+o; ü = z+u */
/*    - ß = F+P;  € = H+,  */
/*  - thumb1 is shift on hold, to ease typing acronyms */

const uint16_t PROGMEM AUML_combo[] = {KC_W, KC_A, COMBO_END};
const uint16_t PROGMEM OUML_combo[] = {KC_Z, KC_O, COMBO_END};
const uint16_t PROGMEM UUML_combo[] = {KC_Z, KC_U, COMBO_END};
const uint16_t PROGMEM EUR_combo[] = {KC_H, KC_COMM, COMBO_END};
const uint16_t PROGMEM SS_combo[] = {KC_F, KC_P, COMBO_END};

combo_t key_combos[] = {
  COMBO(AUML_combo, DE_ADIA),
  COMBO(OUML_combo, DE_ODIA),
  COMBO(UUML_combo, DE_UDIA),
  COMBO(SS_combo, DE_SS),
  COMBO(EUR_combo, DE_EURO),
};
