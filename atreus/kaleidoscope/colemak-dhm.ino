/* -*- mode: c++ -*-
 * Atreus -- Chrysalis-enabled Sketch for the Keyboardio Atreus
 * Copyright (C) 2018, 2019  Keyboard.io, Inc
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/*
* https://colemakmods.github.io/mod-dh/keyboards.html#matrix-keyboards
* german base keys
* Umlauts via Qubit keys
* navigation with vim-like homerow
*/

#ifndef BUILD_INFORMATION
#define BUILD_INFORMATION "locally built on " __DATE__ " at " __TIME__
#endif

#include "Kaleidoscope.h"
#include "Kaleidoscope-EEPROM-Settings.h"
#include "Kaleidoscope-EEPROM-Keymap.h"
#include "Kaleidoscope-FocusSerial.h"
#include "Kaleidoscope-Macros.h"
#include "Kaleidoscope-MouseKeys.h"
#include "Kaleidoscope-OneShot.h"
#include "Kaleidoscope-Qukeys.h"
#include "Kaleidoscope-SpaceCadet.h"
#include "Kaleidoscope-Languages.h"
#include "kaleidoscope/lang/de-qwertz.h"

#define MO(n) ShiftToLayer(n)
#define TG(n) LockLayer(n)

enum {
  MACRO_QWERTY,
  MACRO_VERSION_INFO
};

/* TODO add QWERTZ layer for gaming (and colleagues) */
/* QWERTZ+Shift should be MOVED to (and back) still with ability to use SYMBOLS layer from there */
enum {
  BASE,    // Colemak-DHm: no modifiers
  SYMBOLS, // SUPER key
  NAVI,    // with FUN or Any key (below , and .)
  MOUSE,   // while holding M key
  GAME     // Moved to with ? key
};

/* *INDENT-OFF* */
KEYMAPS(
  [BASE] = KEYMAP_STACKED
  (
   Key_de_Q    ,Key_de_W    ,Key_de_F     ,Key_de_P   ,Key_de_B
  ,Key_de_A    ,Key_de_R    ,Key_de_S     ,Key_de_T   ,Key_de_G
  ,Key_de_Y    ,Key_de_X    ,Key_de_C     ,Key_de_D   ,Key_de_V         ,Key_de_Hash
  ,Key_de_Tab  ,___         ,___          ,Key_de_Delete ,Key_de_Escape ,Key_de_Tab


                     ,Key_de_J     ,Key_de_L          ,Key_de_U      ,Key_de_Z       ,Key_de_Backspace
                     ,Key_de_M     ,Key_de_N          ,Key_de_E      ,Key_de_I       ,Key_de_O
  ,Key_de_LessThan   ,Key_de_K     ,Key_de_H          ,Key_de_Comma  ,Key_de_Period  ,Key_de_Slash
  ,Key_de_Spacebar   ,Key_Enter    ,Key_de_Backspace  ,___           ,TG(GAME)       ,Key_Enter
  ),

  [SYMBOLS] = KEYMAP_STACKED
  (
   Key_de_ExlamationMark  ,Key_de_DoubleQuote  ,Key_de_QuestionMark  ,Key_de_Dollar        ,Key_de_Percent
  ,Key_de_LessThan        ,Key_de_LeftCurly    ,Key_de_LeftParen     ,Key_de_LeftBracket   ,Key_de_Quote
  ,Key_de_GreaterThan     ,Key_de_RightCurly   ,Key_de_RightParen    ,Key_de_RightBracket  ,Key_de_Backslash  ,Key_de_At
  ,___                    ,___                 ,___                  ,Key_de_Pipe          ,Key_de_Tilde      ,Key_de_Ampersand


                        ,Key_de_Circumflex,Key_de_7  ,Key_de_8      ,Key_de_9       ,Key_de_Plus
                        ,Key_de_Asterisk  ,Key_de_4  ,Key_de_5      ,Key_de_6       ,Key_de_Minus
   ,Key_de_Backtick     ,Key_de_0         ,Key_de_1  ,Key_de_2      ,Key_de_3       ,Key_de_Equals
   ,___                 ,___              ,___       ,___           ,___            ,Key_de_Equals
  ),

  [NAVI] = KEYMAP_STACKED
  (
   Key_de_UpArrow     ,Key_de_F7  ,Key_de_F8   ,Key_de_F9   ,Key_de_F10
  ,Key_de_DownArrow   ,Key_de_F4  ,Key_de_F5   ,Key_de_F6   ,Key_de_F11
  ,___                ,Key_de_F1  ,Key_de_F2   ,Key_de_F3   ,Key_de_F12 ,___
  ,___                ,___        ,___         ,___         ,___        ,___


            ,Key_de_Insert      ,Key_de_Home       ,Key_de_UpArrow  ,Key_de_End         ,Key_de_PageUp
            ,Key_de_LeftArrow   ,Key_de_DownArrow  ,Key_de_UpArrow  ,Key_de_RightArrow  ,Key_de_PageDown
   ,___     ,Key_de_VolumeDown  ,Key_de_VolumeUp   ,Key_de_Mute     ,___                ,Consumer_PlaySlashPause
   ,___     ,___                ,___               ,___             ,___                ,___
   ),

  [MOUSE] = KEYMAP_STACKED
  (
   XXX, Key_mouseBtnP, Key_mouseUp, Key_mouseBtnN,      XXX,
   XXX, Key_mouseL,    Key_mouseDn, Key_mouseR,         XXX,
   XXX, XXX,           XXX,         XXX,                XXX, XXX,
   XXX, XXX,           XXX,         XXX, Key_mouseBtnL, Key_mouseScrollUp,

                      XXX,           Key_mouseWarpNW, Key_mouseWarpN,  Key_mouseWarpNE, XXX,
                      XXX,           Key_mouseWarpW,  Key_mouseWarpIn, Key_mouseWarpE,  XXX,
                 XXX, XXX,           Key_mouseWarpSW, Key_mouseWarpS,  Key_mouseWarpSE, XXX,
   Key_mouseScrollDn, Key_mouseBtnR, XXX,             XXX,             XXX, XXX
   ),

  [GAME] = KEYMAP_STACKED
  (
   Key_de_1           ,Key_de_2  ,Key_de_3  ,Key_de_4       ,Key_de_5
  ,Key_de_Tab         ,Key_de_Q  ,Key_de_W  ,Key_de_E       ,Key_de_T
  ,Key_de_LeftShift   ,Key_de_A  ,Key_de_S  ,Key_de_D       ,Key_de_F ,Key_de_6
  ,Key_de_LeftControl ,Key_de_Y  ,Key_de_X  ,Key_de_LeftAlt ,Key_de_C ,Key_de_Spacebar


              ,Key_de_7  ,Key_de_8 ,Key_de_9  ,Key_de_0          ,Key_de_F12
              ,Key_de_G  ,Key_de_L ,Key_de_I  ,Key_de_O          ,Key_de_F11
   ,Key_de_I  ,Key_de_H  ,Key_de_K ,Key_de_U  ,Key_de_K          ,Key_de_Slash
   ,Key_de_B  ,Key_de_M  ,Key_de_N ,Key_de_F5 ,UnlockLayer(GAME) ,Key_Enter
   ),

)
/* *INDENT-ON* */



KALEIDOSCOPE_INIT_PLUGINS(
  EEPROMSettings,
  EEPROMKeymap,
  Focus,
  FocusEEPROMCommand,
  FocusSettingsCommand,
  Qukeys,
  SpaceCadet,
  OneShot,
  Macros,
  MouseKeys
);

const macro_t *macroAction(uint8_t macro_id, KeyEvent &event) {
  if (keyToggledOn(event.state)) {
    switch (macro_id) {
    case MACRO_QWERTY:
      // This macro is currently unused, but is kept around for compatibility
      // reasons. We used to use it in place of `MoveToLayer(QWERTY)`, but no
      // longer do. We keep it so that if someone still has the old layout with
      // the macro in EEPROM, it will keep working after a firmware update.
      Layer.move(BASE);
      break;
    case MACRO_VERSION_INFO:
      Macros.type(PSTR("Keyboardio Atreus - Kaleidoscope "));
      Macros.type(PSTR(BUILD_INFORMATION));
      break;
    default:
      break;
    }
  }
  return MACRO_NONE;
}

void setup() {
  QUKEYS(
    // Qukey(layer, key_addr, alternate_key)

    // HomeRow mods
         // Key_de_LeftAlt
    kaleidoscope::plugin::Qukey(BASE,  KeyAddr(1, 1),  Key_de_LeftAlt),  // left thumb to shift
    kaleidoscope::plugin::Qukey(BASE,  KeyAddr(1, 2),  Key_de_LeftShift),  // left thumb to shift
    kaleidoscope::plugin::Qukey(BASE,  KeyAddr(1, 3),  Key_de_LeftControl),  // bottom left TAB to LCtrl
    kaleidoscope::plugin::Qukey(BASE,  KeyAddr(1, 8),  Key_de_RightControl),  // bottom right Enter to LCtrl
    kaleidoscope::plugin::Qukey(BASE,  KeyAddr(1, 9),  Key_de_RightShift),  // right thumb to shift
    kaleidoscope::plugin::Qukey(BASE,  KeyAddr(1, 10),  Key_de_LeftAlt),  // left thumb to shift

    // Umlauts
    kaleidoscope::plugin::Qukey(BASE,  KeyAddr(1, 2),  Key_de_SS),           // s -> ß
    kaleidoscope::plugin::Qukey(BASE,  KeyAddr(1, 0),  Key_de_AUml),         // a -> ä
    // kaleidoscope::plugin::Qukey(SHIFT, KeyAddr(1, 0),  LSHIFT(Key_de_AUml)), // A -> Ä
    kaleidoscope::plugin::Qukey(BASE,  KeyAddr(1, 11), Key_de_OUml),         // o -> ö
    // kaleidoscope::plugin::Qukey(SHIFT, KeyAddr(1, 11), LSHIFT(Key_de_OUml)), // o -> Ö
    kaleidoscope::plugin::Qukey(BASE,  KeyAddr(0, 9),  Key_de_UUml),         // u -> ü
    // kaleidoscope::plugin::Qukey(SHIFT, KeyAddr(0, 9),  LSHIFT(Key_de_UUml)), // U -> Ü

    //common special characters
    kaleidoscope::plugin::Qukey(BASE,  KeyAddr(0, 0),  Key_de_At),           // q -> @
    kaleidoscope::plugin::Qukey(BASE,  KeyAddr(1, 9),  Key_de_Euro),         // e -> €

    //layer shifts/switches
    kaleidoscope::plugin::Qukey(BASE,  KeyAddr(3, 0),  Key_de_LeftControl),  // bottom left TAB to LCtrl
    kaleidoscope::plugin::Qukey(BASE,  KeyAddr(3, 3),  Key_de_LeftShift),    // left outer thumb to shift
    kaleidoscope::plugin::Qukey(BASE,  KeyAddr(3, 8),  Key_de_RightShift),   // right outer thumb to shift
    kaleidoscope::plugin::Qukey(BASE,  KeyAddr(3, 11), Key_de_RightControl), // bottom right Enter to LCtrl
    kaleidoscope::plugin::Qukey(BASE,  KeyAddr(3, 5),  Key_LeftGui),         // Esc to SUPER
    kaleidoscope::plugin::Qukey(BASE,  KeyAddr(1, 7),  MO(MOUSE)),           // M key to mouse layer
    kaleidoscope::plugin::Qukey(BASE,  KeyAddr(3, 4),  MO(NAVI)),            // ESC to NAVI layer
    kaleidoscope::plugin::Qukey(BASE,  KeyAddr(3, 7),  MO(SYMBOLS)),         // Enter to SYMbol layer

    // hold for "ctrl+" version of the key
    // kaleidoscope::plugin::Qukey(BASE,  KeyAddr(2, 2),  LCTRL(Key_de_C)),     // c copy
    // kaleidoscope::plugin::Qukey(BASE,  KeyAddr(2, 4),  LCTRL(Key_de_V)),     // v paste
    // kaleidoscope::plugin::Qukey(BASE,  KeyAddr(2, 1),  LCTRL(Key_de_X)),     // x cut
    // kaleidoscope::plugin::Qukey(BASE,  KeyAddr(2, 0),  LCTRL(Key_de_Z)),     // z undo
    // kaleidoscope::plugin::Qukey(BASE,  KeyAddr(0, 10), LCTRL(Key_de_Y)),     // y redo
    // kaleidoscope::plugin::Qukey(BASE,  KeyAddr(1, 3),  LCTRL(Key_de_F)),     // f find
    // kaleidoscope::plugin::Qukey(BASE,  KeyAddr(0, 8),  LCTRL(Key_de_L)),     // l select address bar
    // kaleidoscope::plugin::Qukey(BASE,  KeyAddr(1, 1),  LCTRL(Key_de_R)),     // r reload page

    //ease of use
    // kaleidoscope::plugin::Qukey(BASE,  KeyAddr(2, 9),  Key_de_Semicolon),     // semi-colon
    // kaleidoscope::plugin::Qukey(BASE,  KeyAddr(2, 10), Key_de_Colon),         // colon

    //other
    // kaleidoscope::plugin::Qukey(BASE,  KeyAddr(3, 11), Key_de_LeftAlt),       // esc -> alt
  )

  // Qukey settings - this seems somehow calculated different than in QMK
  Qukeys.setHoldTimeout(390);
  Qukeys.setMaxIntervalForTapRepeat(170);
  Qukeys.setOverlapThreshold(80);
  Kaleidoscope.setup();
  SpaceCadet.disable();
  MouseKeys.setSpeedLimit(100);
  MouseKeys.setWarpGridSize(MOUSE_WARP_GRID_3X3);
  EEPROMKeymap.setup(10);
}

void loop() {
  Kaleidoscope.loop();
}
